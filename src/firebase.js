import firebase from "firebase";

import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCCus8t5778c56wukn5l_el9y87i_oBPAM",
    authDomain: "devcamp-r23-t.firebaseapp.com",
    projectId: "devcamp-r23-t",
    storageBucket: "devcamp-r23-t.appspot.com",
    messagingSenderId: "877754890256",
    appId: "1:877754890256:web:ef6f2dedf39a1529b53295"
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider();